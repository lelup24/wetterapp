/*
 * This file is generated by jOOQ.
 */
package de.wetter.backend.data.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.processing.Generated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Wetterdaten implements Serializable {

    private static final long serialVersionUID = 1105638282;

    private Integer id;
    private String  beschreibung;
    private String  icon;
    private Integer ortId;
    private Double  temperatur;
    private Double  gefuehlteTemperatur;
    private Integer minTemperatur;
    private Integer maxTemperatur;
    private Integer luftdruck;
    private Integer feuchtigkeit;
    private Long    sonnenaufgang;
    private Long    sonnenuntergang;
    private Integer bewoelkung;
    private Double  windgeschwindigkeit;
    private Integer windrichtung;
    private Long    anlagedatum;

    public Wetterdaten() {}

    public Wetterdaten(Wetterdaten value) {
        this.id = value.id;
        this.beschreibung = value.beschreibung;
        this.icon = value.icon;
        this.ortId = value.ortId;
        this.temperatur = value.temperatur;
        this.gefuehlteTemperatur = value.gefuehlteTemperatur;
        this.minTemperatur = value.minTemperatur;
        this.maxTemperatur = value.maxTemperatur;
        this.luftdruck = value.luftdruck;
        this.feuchtigkeit = value.feuchtigkeit;
        this.sonnenaufgang = value.sonnenaufgang;
        this.sonnenuntergang = value.sonnenuntergang;
        this.bewoelkung = value.bewoelkung;
        this.windgeschwindigkeit = value.windgeschwindigkeit;
        this.windrichtung = value.windrichtung;
        this.anlagedatum = value.anlagedatum;
    }

    public Wetterdaten(
        Integer id,
        String  beschreibung,
        String  icon,
        Integer ortId,
        Double  temperatur,
        Double  gefuehlteTemperatur,
        Integer minTemperatur,
        Integer maxTemperatur,
        Integer luftdruck,
        Integer feuchtigkeit,
        Long    sonnenaufgang,
        Long    sonnenuntergang,
        Integer bewoelkung,
        Double  windgeschwindigkeit,
        Integer windrichtung,
        Long    anlagedatum
    ) {
        this.id = id;
        this.beschreibung = beschreibung;
        this.icon = icon;
        this.ortId = ortId;
        this.temperatur = temperatur;
        this.gefuehlteTemperatur = gefuehlteTemperatur;
        this.minTemperatur = minTemperatur;
        this.maxTemperatur = maxTemperatur;
        this.luftdruck = luftdruck;
        this.feuchtigkeit = feuchtigkeit;
        this.sonnenaufgang = sonnenaufgang;
        this.sonnenuntergang = sonnenuntergang;
        this.bewoelkung = bewoelkung;
        this.windgeschwindigkeit = windgeschwindigkeit;
        this.windrichtung = windrichtung;
        this.anlagedatum = anlagedatum;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @NotNull
    @Size(max = 100)
    public String getBeschreibung() {
        return this.beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    @NotNull
    @Size(max = 100)
    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @NotNull
    public Integer getOrtId() {
        return this.ortId;
    }

    public void setOrtId(Integer ortId) {
        this.ortId = ortId;
    }

    @NotNull
    public Double getTemperatur() {
        return this.temperatur;
    }

    public void setTemperatur(Double temperatur) {
        this.temperatur = temperatur;
    }

    @NotNull
    public Double getGefuehlteTemperatur() {
        return this.gefuehlteTemperatur;
    }

    public void setGefuehlteTemperatur(Double gefuehlteTemperatur) {
        this.gefuehlteTemperatur = gefuehlteTemperatur;
    }

    @NotNull
    public Integer getMinTemperatur() {
        return this.minTemperatur;
    }

    public void setMinTemperatur(Integer minTemperatur) {
        this.minTemperatur = minTemperatur;
    }

    @NotNull
    public Integer getMaxTemperatur() {
        return this.maxTemperatur;
    }

    public void setMaxTemperatur(Integer maxTemperatur) {
        this.maxTemperatur = maxTemperatur;
    }

    @NotNull
    public Integer getLuftdruck() {
        return this.luftdruck;
    }

    public void setLuftdruck(Integer luftdruck) {
        this.luftdruck = luftdruck;
    }

    @NotNull
    public Integer getFeuchtigkeit() {
        return this.feuchtigkeit;
    }

    public void setFeuchtigkeit(Integer feuchtigkeit) {
        this.feuchtigkeit = feuchtigkeit;
    }

    @NotNull
    public Long getSonnenaufgang() {
        return this.sonnenaufgang;
    }

    public void setSonnenaufgang(Long sonnenaufgang) {
        this.sonnenaufgang = sonnenaufgang;
    }

    @NotNull
    public Long getSonnenuntergang() {
        return this.sonnenuntergang;
    }

    public void setSonnenuntergang(Long sonnenuntergang) {
        this.sonnenuntergang = sonnenuntergang;
    }

    @NotNull
    public Integer getBewoelkung() {
        return this.bewoelkung;
    }

    public void setBewoelkung(Integer bewoelkung) {
        this.bewoelkung = bewoelkung;
    }

    @NotNull
    public Double getWindgeschwindigkeit() {
        return this.windgeschwindigkeit;
    }

    public void setWindgeschwindigkeit(Double windgeschwindigkeit) {
        this.windgeschwindigkeit = windgeschwindigkeit;
    }

    @NotNull
    public Integer getWindrichtung() {
        return this.windrichtung;
    }

    public void setWindrichtung(Integer windrichtung) {
        this.windrichtung = windrichtung;
    }

    @NotNull
    public Long getAnlagedatum() {
        return this.anlagedatum;
    }

    public void setAnlagedatum(Long anlagedatum) {
        this.anlagedatum = anlagedatum;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Wetterdaten (");

        sb.append(id);
        sb.append(", ").append(beschreibung);
        sb.append(", ").append(icon);
        sb.append(", ").append(ortId);
        sb.append(", ").append(temperatur);
        sb.append(", ").append(gefuehlteTemperatur);
        sb.append(", ").append(minTemperatur);
        sb.append(", ").append(maxTemperatur);
        sb.append(", ").append(luftdruck);
        sb.append(", ").append(feuchtigkeit);
        sb.append(", ").append(sonnenaufgang);
        sb.append(", ").append(sonnenuntergang);
        sb.append(", ").append(bewoelkung);
        sb.append(", ").append(windgeschwindigkeit);
        sb.append(", ").append(windrichtung);
        sb.append(", ").append(anlagedatum);

        sb.append(")");
        return sb.toString();
    }
}
