package de.wetter.backend.wetter.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import de.wetter.backend.wetter.beobachtungen.WetterBeobachtungService;

public class WetterStuendlichJob implements Job {
	
	private Logger logger = LoggerFactory.getLogger(WetterStuendlichJob.class);
	
	@Autowired
	private WetterBeobachtungService wetterBeobachtungService;
	
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("Stündliche Wetterabfrage wird ausgeführt");
		wetterBeobachtungService.ladeWetterDaten("Dankerode", "DE", "metric");	
	}

}
