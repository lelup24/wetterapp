package de.wetter.backend.wetter.beobachtungen;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.wetter.backend.global.annotationen.ApiRestController;

@ApiRestController
public class WetterBeobachtungController {
	
	private WetterBeobachtungService wetterBeobachtungService;

	public WetterBeobachtungController(WetterBeobachtungService wetterBeobachtungService) {
		super();
		this.wetterBeobachtungService = wetterBeobachtungService;
	}
	
	@GetMapping("/beobachtungen/{ort}")
	public ResponseEntity<List<BeobachtungModel>> ladeWetterBeobachtungenByOrt(@PathVariable String ort) {
		return new ResponseEntity<List<BeobachtungModel>>(wetterBeobachtungService.ladeBeobachtungenByOrt(ort), HttpStatus.OK);
	}

}
