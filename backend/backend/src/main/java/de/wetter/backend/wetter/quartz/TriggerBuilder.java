package de.wetter.backend.wetter.quartz;

import java.util.Calendar;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

public class TriggerBuilder {
	
	    static SimpleTriggerFactoryBean buildSimpleTrigger(JobDetail job, String triggerName) {

	        SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
	        trigger.setJobDetail(job);

	        int frequencyInSec = 2;

	        trigger.setRepeatInterval(frequencyInSec * 1000);
	        trigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
	        trigger.setName(triggerName);
	        return trigger;
	    }
	    
	    static  CronTriggerFactoryBean buildConTrigger(JobDetail jobDetail, String triggerName, String expression) {
	    	CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
	    	
	    	Calendar calendar = Calendar.getInstance();
	        calendar.set(Calendar.SECOND, 0);
	        calendar.set(Calendar.MILLISECOND, 0);
	    	
	    	factoryBean.setJobDetail(jobDetail);
	        factoryBean.setCronExpression(expression);
	        factoryBean.setStartTime(calendar.getTime());
	        factoryBean.setStartDelay(0L);
	        factoryBean.setName(triggerName);
	        factoryBean.setMisfireInstruction(CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING);

	    	return factoryBean;
	    }

}
