package de.wetter.backend.wetter.beobachtungen;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectJoinStep;
import org.springframework.stereotype.Component;

import static org.jooq.impl.DSL.noCondition;

import static de.wetter.backend.data.jooq.tables.Ort.ORT;
import static de.wetter.backend.data.jooq.tables.Wetterdaten.WETTERDATEN;

import java.util.List;

@Component
class WetterBeobachtungFinder {
	
	private DSLContext dsl;

	/**
	 * @param dsl
	 */
	WetterBeobachtungFinder(DSLContext dsl) {
		this.dsl = dsl;
	}
	
	public Boolean ortExistiert(Integer ortsId) {
		return dsl
				.fetchExists(dsl.select().from(ORT).where(ORT.ID.eq(ortsId)));
	}
	
	private Condition addCondition(String ort) {
		return noCondition().and(ORT.NAME.eq(ort));
	}
	private SelectJoinStep<Record> addSelectJoinStep() {
		return dsl.select()
				.from(ORT)
				.join(WETTERDATEN)
				.on(WETTERDATEN.ORT_ID.eq(ORT.ID));
	}
	
	public List<BeobachtungModel> ladeBeobachtungenByOrt(String ort) {
		
		final SelectJoinStep<Record> select = addSelectJoinStep();
		final Condition whereCondition = addCondition(ort);
		
		return select
				.where(whereCondition)
				.fetch()
				.map(BeobachtungModelMapper::map);
	}


}
