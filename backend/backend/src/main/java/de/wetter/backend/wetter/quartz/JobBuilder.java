package de.wetter.backend.wetter.quartz;

import org.quartz.Job;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

public class JobBuilder {

	public static JobDetailFactoryBean build(Class<? extends Job> klasse, String jobName) {

        JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(klasse);
        jobDetailFactory.setName(jobName);
        //jobDetailFactory.setDescription("Lädt alle 60 Minuten die Wetterinformationen von 'openweathermap.org");
        jobDetailFactory.setDurability(true);
        return jobDetailFactory;
    }
}
