package de.wetter.backend.wetter.beobachtungen;

import java.util.List;

class BeobachtungModel {
	
	private CoordsModel coord;
	private List<WeatherModel> weather;
	private String base;
	private MainModel main;
	private Integer visibility;
	private WindModel wind;
	private CloudsModel clouds;
	private Long dt;
	private SysModel sys;
	private Integer timezone;
	private Integer id;
	private String name;
	private Integer cod;
	private Long anlagedatum;
	
	
	public CoordsModel getCoord() {
		return coord;
	}
	public BeobachtungModel setCoords(CoordsModel coord) {
		this.coord = coord;
		return this;
	}
	public List<WeatherModel> getWeather() {
		return weather;
	}
	public BeobachtungModel setWeather(List<WeatherModel> weather) {
		this.weather = weather;
		return this;
	}
	public String getBase() {
		return base;
	}
	public BeobachtungModel setBase(String base) {
		this.base = base;
		return this;
	}
	public MainModel getMain() {
		return main;
	}
	public BeobachtungModel setMain(MainModel main) {
		this.main = main;
		return this;
	}
	public Integer getVisibility() {
		return visibility;
	}
	public BeobachtungModel setVisibility(Integer visibility) {
		this.visibility = visibility;
		return this;
	}
	public WindModel getWind() {
		return wind;
	}
	public BeobachtungModel setWind(WindModel wind) {
		this.wind = wind;
		return this;
	}
	public CloudsModel getClouds() {
		return clouds;
	}
	public BeobachtungModel setClouds(CloudsModel clouds) {
		this.clouds = clouds;
		return this;
	}
	public Long getDt() {
		return dt;
	}
	public BeobachtungModel setDt(Long dt) {
		this.dt = dt;
		return this;
	}
	public SysModel getSys() {
		return sys;
	}
	public BeobachtungModel setSys(SysModel sys) {
		this.sys = sys;
		return this;
	}
	public Integer getTimezone() {
		return timezone;
	}
	public BeobachtungModel setTimezone(Integer timezone) {
		this.timezone = timezone;
		return this;
	}
	public Integer getId() {
		return id;
	}
	public BeobachtungModel setId(Integer id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public BeobachtungModel setName(String name) {
		this.name = name;
		return this;
	}
	public Integer getCod() {
		return cod;
	}
	public BeobachtungModel setCod(Integer cod) {
		this.cod = cod;
		return this;
	}
	public Long getAnlagedatum() {
		return anlagedatum;
	}
	public BeobachtungModel setAnlagedatum(Long anlagedatum) {
		this.anlagedatum = anlagedatum;
		return this;
	}
	
	

}
