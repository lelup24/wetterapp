package de.wetter.backend.wetter.beobachtungen;

public class WindModel {
	
	private Float speed;
	private Integer deg;
	
	public Float getSpeed() {
		return speed;
	}
	public void setSpeed(Float speed) {
		this.speed = speed;
	}
	public Integer getDeg() {
		return deg;
	}
	public void setDeg(Integer deg) {
		this.deg = deg;
	}
	
	

}
