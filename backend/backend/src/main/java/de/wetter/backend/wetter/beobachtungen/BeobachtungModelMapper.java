package de.wetter.backend.wetter.beobachtungen;

import static de.wetter.backend.data.jooq.tables.Ort.ORT;
import static de.wetter.backend.data.jooq.tables.Wetterdaten.WETTERDATEN;

import java.util.Collections;

import org.jooq.Record;

import de.wetter.backend.data.jooq.tables.records.OrtRecord;
import de.wetter.backend.data.jooq.tables.records.WetterdatenRecord;

class BeobachtungModelMapper {
	

	public static BeobachtungModel map(Record fromRecord) {
		
		OrtRecord ortsRecord = fromRecord.into(ORT);
		WetterdatenRecord wetterdatenRecord = fromRecord.into(WETTERDATEN);
		
		return new BeobachtungModel()
				.setBase(ortsRecord.getBase())
				.setClouds(BeobachtungModelMapper.mapWolken(wetterdatenRecord))
				.setCoords(mapCoords(ortsRecord))
				.setDt(null)
				.setMain(mapMain(wetterdatenRecord))
				.setSys(mapSys(wetterdatenRecord))
				.setWeather(Collections.singletonList(mapWeather(wetterdatenRecord)))
				.setWind(mapWindModel(wetterdatenRecord))
				.setName(ortsRecord.getName())
				.setAnlagedatum(wetterdatenRecord.getAnlagedatum())
				;
	}
	
	private static CloudsModel mapWolken(WetterdatenRecord record) {
		CloudsModel model = new CloudsModel();
		model.setAll(record.getBewoelkung());
		return model;
		
	}
	
	private static CoordsModel mapCoords(OrtRecord record) {
		CoordsModel model = new CoordsModel();
		model.setLat(record.getBreitengrad());
		model.setLon(record.getLaengengrad());
		return model;
	}
	
	private static MainModel mapMain(WetterdatenRecord record) {
		MainModel mainModel = new MainModel();
		mainModel.setFeels_like(record.getGefuehlteTemperatur());
		mainModel.setHumidity(record.getFeuchtigkeit());
		mainModel.setPressure(record.getLuftdruck());
		mainModel.setTemp(record.getTemperatur());
		mainModel.setTemp_max(record.getMaxTemperatur());
		mainModel.setTemp_min(record.getMinTemperatur());
		return mainModel;
	}
	
	private static SysModel mapSys(WetterdatenRecord record) {
		SysModel sysModel = new SysModel();
		sysModel.setSunrise(record.getSonnenaufgang());
		sysModel.setSunset(record.getSonnenuntergang());
		return sysModel;
	}
	
	private static WeatherModel mapWeather(WetterdatenRecord record) {
		WeatherModel weatherModel = new WeatherModel();
		weatherModel.setDescription(record.getBeschreibung());
		weatherModel.setIconString(record.getIcon());
		weatherModel.setMain(weatherModel.getMain());
		return weatherModel;
	}
	
	private static WindModel mapWindModel(WetterdatenRecord record) {
		WindModel windModel = new WindModel();
		return windModel;
	}

}
