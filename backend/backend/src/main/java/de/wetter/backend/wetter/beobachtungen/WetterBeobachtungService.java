package de.wetter.backend.wetter.beobachtungen;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import de.wetter.backend.data.jooq.tables.daos.OrtDao;
import de.wetter.backend.data.jooq.tables.daos.WetterdatenDao;
import de.wetter.backend.data.jooq.tables.pojos.Ort;
import de.wetter.backend.data.jooq.tables.pojos.Wetterdaten;

@Service
public class WetterBeobachtungService {
	
	@Value("${API_KEY}")
	private String apiKey;
	
	private OrtDao ortDao;
	private WetterdatenDao wetterdatenDao;
	
	private final WetterBeobachtungFinder wetterBeobachtungFinder;
	
	
	/**
	 * @param ortDao
	 * @param datenDao
	 * @param wetterDao
	 * @param koordinatenDao
	 */
	public WetterBeobachtungService(final OrtDao ortDao, final WetterBeobachtungFinder wetterBeobachtungFinder, final WetterdatenDao wetterdatenDao) {
		this.ortDao = ortDao;
		this.wetterdatenDao = wetterdatenDao;
		this.wetterBeobachtungFinder = wetterBeobachtungFinder;
	}

	private Optional<BeobachtungModel> stelleAnfrage(String city, String lang, String units) {
		
		String url = UriComponentsBuilder
	    .fromUriString("https://api.openweathermap.org/data/2.5/weather")
	    .queryParam("q", city)
	    .queryParam("appid", apiKey)
	    .queryParam("lang", lang)
	    .queryParam("units", units)
	    .build().toUriString();
		
		final RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<BeobachtungModel> response = restTemplate.getForEntity(url, BeobachtungModel.class);
		
		return Optional.of(response.getBody());
	}
	
	public void ladeWetterDaten(String city, String lang, String units) {
		Optional<BeobachtungModel> modelOptional = stelleAnfrage(city, lang, units);
		
		modelOptional.ifPresentOrElse(model -> {
			wetterDatenVerarbeiten(model);
		}, () -> { throw new RuntimeException("Keine Werte für diesen Ort gefunden oder Anfrage nicht möglich."); });
	}
	
	private void wetterDatenVerarbeiten(final BeobachtungModel model) {
		
		if (wetterBeobachtungFinder.ortExistiert(model.getId())) {
			wetterDatenZuOrtEintragen(model);
		} else {
			ortAnlegen(model);
		}
		
		
	}
	
	private void ortAnlegen(final BeobachtungModel model) {
		
		System.out.println(model.getName());
		
		Ort ort = new Ort();
		ort.setId(model.getId());
		ort.setBase(model.getBase());
		ort.setLand(model.getSys().getCountry());
		ort.setName(model.getName());
		ort.setSichtbarkeit(model.getVisibility());
		ort.setZeitzone(model.getTimezone());
		ort.setLaengengrad(model.getCoord().getLon());
		ort.setBreitengrad(model.getCoord().getLat());
		ort.setAnlagedatum(System.currentTimeMillis());
		ortDao.insert(ort);
		

		
		Wetterdaten daten = new Wetterdaten();
		daten.setBewoelkung(model.getClouds().getAll());
		daten.setFeuchtigkeit(model.getMain().getHumidity());
		daten.setGefuehlteTemperatur(model.getMain().getFeels_like());
		daten.setLuftdruck(model.getMain().getPressure());
		daten.setMaxTemperatur(model.getMain().getTemp_max());
		daten.setMinTemperatur(model.getMain().getTemp_min());
		daten.setSonnenaufgang(model.getSys().getSunrise());
		daten.setSonnenuntergang(model.getSys().getSunset());
		daten.setTemperatur(model.getMain().getTemp());
		daten.setBeschreibung(model.getWeather().get(0).getDescription());
		daten.setIcon(model.getWeather().get(0).getIcon());
		daten.setOrtId(ort.getId());
		daten.setAnlagedatum(System.currentTimeMillis());
		daten.setWindgeschwindigkeit(Double.valueOf(model.getWind().getSpeed()));
		daten.setWindrichtung(model.getWind().getDeg());
		wetterdatenDao.insert(daten);
	}
	
	private void wetterDatenZuOrtEintragen(BeobachtungModel model) {
		
		Wetterdaten daten = new Wetterdaten();
		daten.setBewoelkung(model.getClouds().getAll());
		daten.setFeuchtigkeit(model.getMain().getHumidity());
		daten.setGefuehlteTemperatur(model.getMain().getFeels_like());
		daten.setLuftdruck(model.getMain().getPressure());
		daten.setMaxTemperatur(model.getMain().getTemp_max());
		daten.setMinTemperatur(model.getMain().getTemp_min());
		daten.setSonnenaufgang(model.getSys().getSunrise());
		daten.setSonnenuntergang(model.getSys().getSunset());
		daten.setTemperatur(model.getMain().getTemp());
		daten.setBeschreibung(model.getWeather().get(0).getDescription());
		daten.setIcon(model.getWeather().get(0).getIcon());
		daten.setOrtId(model.getId());
		daten.setAnlagedatum(System.currentTimeMillis());
		wetterdatenDao.insert(daten);
	}
	
	public List<BeobachtungModel> ladeBeobachtungenByOrt(String ort) {
		return wetterBeobachtungFinder.ladeBeobachtungenByOrt(ort);
	}

}
