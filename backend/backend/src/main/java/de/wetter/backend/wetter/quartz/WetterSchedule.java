package de.wetter.backend.wetter.quartz;

import javax.sql.DataSource;

import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;


@Configuration
public class WetterSchedule {

    private static final String CRON_EVERY_FIVE_MINUTES = "0 0 0/1 ? * * *";

    @Autowired
    SpringBeanJobFactory springBeanJobFactory;

    @Bean
    public SchedulerFactoryBean scheduler(DataSource quartzDataSource, Trigger... triggers) {

        SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
        schedulerFactory.setConfigLocation(new ClassPathResource("quartz.properties"));

        schedulerFactory.setOverwriteExistingJobs(true);
        schedulerFactory.setAutoStartup(true);
        schedulerFactory.setDataSource(quartzDataSource);
        schedulerFactory.setJobFactory(springBeanJobFactory);
        schedulerFactory.setWaitForJobsToCompleteOnShutdown(true);
        schedulerFactory.setSchedulerName("Wetter-Schedule");
        schedulerFactory.setTriggers(triggers);

        return schedulerFactory;
    }
    
    @Bean(name = "wetterStuendlich")
    public JobDetailFactoryBean wetterStuendlich() {
        return JobBuilder.build(WetterStuendlichJob.class, "Wetter-Stündlich");
    }
    
    @Bean
    public CronTriggerFactoryBean simple2(@Qualifier("wetterStuendlich") JobDetail jobDetail) {
    	return TriggerBuilder.buildConTrigger(jobDetail, "Peter", CRON_EVERY_FIVE_MINUTES);
    }
    
    

}