CREATE DATABASE IF NOT EXISTS `wetterdb`;

USE `wetterdb`;

CREATE TABLE IF NOT EXISTS `ort` 
(
	id INT NOT NULL,
	base VARCHAR(100) NOT NULL,
	name VARCHAR(100) NOT NULL,
	laengengrad float NOT NULL,
	breitengrad float NOT NULL,
	zeitzone INT NOT NULL,
	sichtbarkeit INT NOT NULL,
	land VARCHAR(100) NOT NULL,
	anlagedatum bigint NOT NULL,
	
	PRIMARY KEY (id)
	
);

CREATE TABLE IF NOT EXISTS  `wetterdaten` 
(
	id INT AUTO_INCREMENT,
	beschreibung VARCHAR(100) NOT NULL,
	icon VARCHAR(100) NOT NULL,
	ort_id INT NOT NULL,
	temperatur float NOT NULL,
	gefuehlte_temperatur float NOT NULL,
	min_temperatur INT NOT NULL,
	max_temperatur INT NOT NULL,
	luftdruck INT NOT NULL,
	feuchtigkeit INT NOT NULL,
	sonnenaufgang bigint NOT NULL,
	sonnenuntergang bigint NOT NULL,
	bewoelkung INT NOT NULL,
	windgeschwindigkeit FLOAT NOT NULL,
	windrichtung INT NOT NULL,
	anlagedatum bigint NOT NULL,
	
	PRIMARY KEY (id),
	
	CONSTRAINT FK_wetter_ort
	FOREIGN KEY (ort_id) REFERENCES `ort`(id)
);

GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost';

