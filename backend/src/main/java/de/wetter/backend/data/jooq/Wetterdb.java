/*
 * This file is generated by jOOQ.
 */
package de.wetter.backend.data.jooq;


import de.wetter.backend.data.jooq.tables.Daten;
import de.wetter.backend.data.jooq.tables.Koordinaten;
import de.wetter.backend.data.jooq.tables.Ort;
import de.wetter.backend.data.jooq.tables.Wetter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.processing.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Wetterdb extends SchemaImpl {

    private static final long serialVersionUID = -1009988469;

    /**
     * The reference instance of <code>wetterdb</code>
     */
    public static final Wetterdb WETTERDB = new Wetterdb();

    /**
     * The table <code>wetterdb.daten</code>.
     */
    public final Daten DATEN = de.wetter.backend.data.jooq.tables.Daten.DATEN;

    /**
     * The table <code>wetterdb.koordinaten</code>.
     */
    public final Koordinaten KOORDINATEN = de.wetter.backend.data.jooq.tables.Koordinaten.KOORDINATEN;

    /**
     * The table <code>wetterdb.ort</code>.
     */
    public final Ort ORT = de.wetter.backend.data.jooq.tables.Ort.ORT;

    /**
     * The table <code>wetterdb.wetter</code>.
     */
    public final Wetter WETTER = de.wetter.backend.data.jooq.tables.Wetter.WETTER;

    /**
     * No further instances allowed
     */
    private Wetterdb() {
        super("wetterdb", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Daten.DATEN,
            Koordinaten.KOORDINATEN,
            Ort.ORT,
            Wetter.WETTER);
    }
}
