/*
 * This file is generated by jOOQ.
 */
package de.wetter.backend.data.jooq.tables;


import de.wetter.backend.data.jooq.Indexes;
import de.wetter.backend.data.jooq.Keys;
import de.wetter.backend.data.jooq.Wetterdb;
import de.wetter.backend.data.jooq.tables.records.DatenRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row11;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Daten extends TableImpl<DatenRecord> {

    private static final long serialVersionUID = 243301611;

    /**
     * The reference instance of <code>wetterdb.daten</code>
     */
    public static final Daten DATEN = new Daten();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<DatenRecord> getRecordType() {
        return DatenRecord.class;
    }

    /**
     * The column <code>wetterdb.daten.id</code>.
     */
    public final TableField<DatenRecord, Integer> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>wetterdb.daten.temperatur</code>.
     */
    public final TableField<DatenRecord, Integer> TEMPERATUR = createField(DSL.name("temperatur"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.gefuehlte_temperatur</code>.
     */
    public final TableField<DatenRecord, Integer> GEFUEHLTE_TEMPERATUR = createField(DSL.name("gefuehlte_temperatur"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.min_temperatur</code>.
     */
    public final TableField<DatenRecord, Integer> MIN_TEMPERATUR = createField(DSL.name("min_temperatur"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.max_temperatur</code>.
     */
    public final TableField<DatenRecord, Integer> MAX_TEMPERATUR = createField(DSL.name("max_temperatur"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.luftdruck</code>.
     */
    public final TableField<DatenRecord, Integer> LUFTDRUCK = createField(DSL.name("luftdruck"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.feuchtigkeit</code>.
     */
    public final TableField<DatenRecord, Integer> FEUCHTIGKEIT = createField(DSL.name("feuchtigkeit"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.sonnenaufgang</code>.
     */
    public final TableField<DatenRecord, Integer> SONNENAUFGANG = createField(DSL.name("sonnenaufgang"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.sonnenuntergang</code>.
     */
    public final TableField<DatenRecord, Integer> SONNENUNTERGANG = createField(DSL.name("sonnenuntergang"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.bewoelkung</code>.
     */
    public final TableField<DatenRecord, Integer> BEWOELKUNG = createField(DSL.name("bewoelkung"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>wetterdb.daten.wetter_id</code>.
     */
    public final TableField<DatenRecord, Integer> WETTER_ID = createField(DSL.name("wetter_id"), org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>wetterdb.daten</code> table reference
     */
    public Daten() {
        this(DSL.name("daten"), null);
    }

    /**
     * Create an aliased <code>wetterdb.daten</code> table reference
     */
    public Daten(String alias) {
        this(DSL.name(alias), DATEN);
    }

    /**
     * Create an aliased <code>wetterdb.daten</code> table reference
     */
    public Daten(Name alias) {
        this(alias, DATEN);
    }

    private Daten(Name alias, Table<DatenRecord> aliased) {
        this(alias, aliased, null);
    }

    private Daten(Name alias, Table<DatenRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Daten(Table<O> child, ForeignKey<O, DatenRecord> key) {
        super(child, key, DATEN);
    }

    @Override
    public Schema getSchema() {
        return Wetterdb.WETTERDB;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.DATEN_FK_DATEN_WETTER, Indexes.DATEN_PRIMARY);
    }

    @Override
    public Identity<DatenRecord, Integer> getIdentity() {
        return Keys.IDENTITY_DATEN;
    }

    @Override
    public UniqueKey<DatenRecord> getPrimaryKey() {
        return Keys.KEY_DATEN_PRIMARY;
    }

    @Override
    public List<UniqueKey<DatenRecord>> getKeys() {
        return Arrays.<UniqueKey<DatenRecord>>asList(Keys.KEY_DATEN_PRIMARY);
    }

    @Override
    public List<ForeignKey<DatenRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<DatenRecord, ?>>asList(Keys.FK_DATEN_WETTER);
    }

    public Wetter wetter() {
        return new Wetter(this, Keys.FK_DATEN_WETTER);
    }

    @Override
    public Daten as(String alias) {
        return new Daten(DSL.name(alias), this);
    }

    @Override
    public Daten as(Name alias) {
        return new Daten(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Daten rename(String name) {
        return new Daten(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Daten rename(Name name) {
        return new Daten(name, null);
    }

    // -------------------------------------------------------------------------
    // Row11 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row11<Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer> fieldsRow() {
        return (Row11) super.fieldsRow();
    }
}
