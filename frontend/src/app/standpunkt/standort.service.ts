import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Standort } from './models/standort.model';

@Injectable({
	providedIn: 'root'
})
export class StandortService {

    private standpunkt: BehaviorSubject<any|null> = new BehaviorSubject<any|null>(null);

	private ermittleGegenaertigenStandpunkt(): Promise<Standort> {
		if (navigator.geolocation) {
			 navigator.geolocation.getCurrentPosition((position) => {
			this.standpunkt.next(position);
		});
		} else {
			alert('Standpunkt konnte nicht ermittelt werden. Haben Sie GPS zugelassen?');
			return;
		}
	}

	public get jetzigenStandpunkt() {
		this.ermittleGegenaertigenStandpunkt();
		return this.standpunkt;
	}

}
