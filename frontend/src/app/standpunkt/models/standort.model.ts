export interface Standort {
    coords: {
        latitude: number;
        longitude: number;
        altitude: number;
        accuracy: number;
        altitudeAccuracy: number;
        heading: any;
        speed: number;
    };
}
