import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Wetter } from './models/wetter.model';
import { WetterVorschauModel } from './models/wetter-vorschau.model';

const BASE = 'https://api.openweathermap.org/data/2.5';

@Injectable({
	providedIn: 'root'
})
export class WetterService {

	constructor(private http: HttpClient) { }

	public ladeWetterByCity(city: string): Observable<Wetter> {
		return this.http
		 .get<Wetter>(`${BASE}/weather?q=${city}&appid=${environment.wetterKey}&lang=de&units=metric`);
	}

	public ladeWetterByStandort(laengengrad: number, breitengrad: number): Observable<Wetter> {
		return this.http
		 .get<Wetter>
		 (`${BASE}/weather?lat=${breitengrad}` +
		 `&lon=${laengengrad}&appid=${environment.wetterKey}&lang=de&units=metric`);
	}

	public ladeVorschauByStandort(laengengrad: number, breitengrad: number): Observable<WetterVorschauModel> {
		return this.http.get<WetterVorschauModel>(`${BASE}/forecast?lat=${breitengrad}` +
		 `&lon=${laengengrad}&appid=${environment.wetterKey}&lang=de&units=metric`);
	}

	public ladeVorschauByOrt(city: string): Observable<WetterVorschauModel> {
		return this.http.get<WetterVorschauModel>(`${BASE}/forecast?` +
		`q=${city}&appid=${environment.wetterKey}&lang=de&units=metric`);
	}


}
