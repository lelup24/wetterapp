export interface Wetter {
	base: string;
	coords: {
            lat: number;
            lon: number;
        };
	visibility: number;
	dt: number;
	timezone: number;
	id: number;
	name: string;
	cod: number;
	weather: [{
		id: number;
		main: string;
		description: string;
		icon: string;
	}];
	wind: {
		speed: number;
		deg: number;
		gust: number;
	};
	clouds: {
		 all: number;
	};
	sys: {
		type: number;
		id: number;
		country: string;
		sunrise: number;
		sunset: number;
	};
	main: {
		temp: number;
		feels_like: number;
		temp_min: number;
		temp_max: number;
		pressure: number;
		humidity: number;
	};
}
