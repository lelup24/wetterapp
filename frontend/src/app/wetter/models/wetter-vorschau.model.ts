import { WetterDaten } from './wetter-daten.model';

export interface WetterVorschauModel {
    cod: string;
    message: number;
    cnt: string;
    city: {
        id: number;
        name: string;
        coord: {
            lat: number;
            lon: number;
        };
        county: string;
        population: number
    };
    list: WetterDaten[];
}
