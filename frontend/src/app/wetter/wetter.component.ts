import { Component, OnInit, OnDestroy } from '@angular/core';
import { WetterService } from './wetter.service';
import { Subscription } from 'rxjs';
import { debounceTime, } from 'rxjs/operators';
import { StandortService } from '../standpunkt/standort.service';
import { Wetter } from './models/wetter.model';
import { WetterVorschauModel } from './models/wetter-vorschau.model';


@Component({
  selector: 'app-wetter',
  templateUrl: './wetter.component.html',
  styleUrls: ['./wetter.component.scss']
})
export class WetterComponent implements OnInit, OnDestroy {

  private sub: Subscription;
  public wetter: Wetter;
  public vorschau: WetterVorschauModel;
  public ort: string;
  public lade = false;

  constructor(private wetterService: WetterService, private standort: StandortService) { }

  ngOnInit(): void {
    this.ladeWetterByKoordinaten();
    this.ladeVorschauByKoordinaten();
  }

  ngOnDestroy(): void {
	  this.sub.unsubscribe();
  }


  public ladeWetterByOrt() {
     this.sub = this.wetterService.ladeWetterByCity(this.ort)
		  .subscribe(res => {
		  this.wetter = res;
    });
     this.wetterService.ladeVorschauByOrt(this.ort).subscribe(res => {
      this.vorschau = res;
    });
  }

  private ladeWetterByKoordinaten() {
    this.sub = this.standort.jetzigenStandpunkt
    .pipe(
     debounceTime(200)
    )
    .subscribe(pos => { pos != null ?
      this.sub = this.wetterService.ladeWetterByStandort
      (pos.coords.longitude, pos.coords.latitude)
      .subscribe(res => { this.wetter = res; },
        (err) => { alert(err); },
        () => this.lade = true
        )
      : this.lade = true;
    });
  }

  private ladeVorschauByKoordinaten() {
    this.sub = this.standort.jetzigenStandpunkt
     .pipe(
      debounceTime(200)
    )
    .subscribe(pos => { pos != null ?
      this.sub = this.wetterService.ladeVorschauByStandort
      (pos.coords.longitude, pos.coords.latitude)
      .subscribe(res => { this.vorschau = res; },
        (err) => { alert(err); },
        () => this.lade = true
        )
      : this.lade = true;
    });
  }

}
