import { Component, OnInit, Input } from '@angular/core';
import { Wetter } from '../models/wetter.model';

@Component({
  selector: 'app-zusammenfassung-wetter',
  templateUrl: './zusammenfassung-wetter.component.html',
  styleUrls: ['./zusammenfassung-wetter.component.scss']
})
export class ZusammenfassungWetterComponent implements OnInit {
  @Input() wetter: Wetter;

  constructor() { }

  ngOnInit(): void {
  }

}
