import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZusammenfassungWetterComponent } from './zusammenfassung-wetter.component';

describe('ZusammenfassungWetterComponent', () => {
  let component: ZusammenfassungWetterComponent;
  let fixture: ComponentFixture<ZusammenfassungWetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZusammenfassungWetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZusammenfassungWetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
