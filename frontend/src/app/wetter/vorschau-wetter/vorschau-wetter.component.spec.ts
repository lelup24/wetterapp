import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VorschauWetterComponent } from './vorschau-wetter.component';

describe('VorschauWetterComponent', () => {
  let component: VorschauWetterComponent;
  let fixture: ComponentFixture<VorschauWetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VorschauWetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VorschauWetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
