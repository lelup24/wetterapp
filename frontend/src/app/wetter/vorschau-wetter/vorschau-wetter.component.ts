import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Label } from 'ng2-charts';
import { WetterVorschauModel } from '../models/wetter-vorschau.model';

@Component({
  selector: 'app-vorschau-wetter',
  templateUrl: './vorschau-wetter.component.html',
  styleUrls: ['./vorschau-wetter.component.scss']
})
export class VorschauWetterComponent implements OnInit, OnChanges {

  @Input() vorschauDaten: WetterVorschauModel;

   public options = {
    scaleSowVerticalines: false,
    responsive: true,
     scales: {
     yAxes: [{
            ticks: {
              beginAtZero: false
            }
          }]
        }

  };

  public chartType = 'line';

  public lineData =
  [{
    data: [],
    label: ['Temperatur']
  },
  {
    data: [],
    label: ['Luftfeuchtigkeit']
  }
];

  public labels: Label[] = [];
  public legend = true;


  public chartColors: any[] = [
      {
        backgroundColor: ['rgba(255,99,132,0.6)', 'rgba(54,162,235,0.6)', 'rgba(255,206,86,0.6)',
        'rgba(231,233,237,0.6)', 'rgba(75,192,192,0.6)', 'rgba(151,187,205,0.6)', 'rgba(220,220,220,0.6)']
      }];



  constructor() { }

  ngOnInit(): void {
   this.ladeVorschau();
  }

  ngOnChanges() {
    this.ladeVorschau();
  }

  private ladeVorschau() {
    const newData = [];
    const newLabels = [];
    const luftFeuchtigkeit = [];
    this.vorschauDaten.list.forEach(data => { newLabels.push(new Date(data.dt_txt).getHours() + ' Uhr'),
                                              newData.push(data.main.temp);
                                              luftFeuchtigkeit.push(data.main.humidity);
    });
    this.lineData[0].data = newData.slice(0, 9);
    this.labels = newLabels.slice(0, 9);
    this.lineData[1].data = luftFeuchtigkeit.slice(0, 9);
  }

}
