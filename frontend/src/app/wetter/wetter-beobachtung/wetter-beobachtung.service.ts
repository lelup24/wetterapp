import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Wetter } from '../models/wetter.model';

const BASE_URL = '/api/beobachtungen';

@Injectable({
    providedIn: 'root'
})
export class WetterBeoachtungService {

    constructor(private http: HttpClient) {}

    public ladeBeobachtungenByOrt(ort: string): Observable<Wetter[]> {
        return this.http.get<Wetter[]>(BASE_URL + `/${ort}`);
    }
}

