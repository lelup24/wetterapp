import { Component, OnInit } from '@angular/core';
import { WetterBeoachtungService } from './wetter-beobachtung.service';
import { Wetter } from '../models/wetter.model';

@Component({
  selector: 'app-wetter-beobachtung',
  templateUrl: './wetter-beobachtung.component.html',
  styleUrls: ['./wetter-beobachtung.component.scss']
})
export class WetterBeobachtungComponent implements OnInit {


  public beobachtungen: Wetter[];

  constructor(private beobachtungService: WetterBeoachtungService) { }

  ngOnInit(): void {
    this.beobachtungService.ladeBeobachtungenByOrt('Dankerode').subscribe(res => this.beobachtungen = res);
  }

}
