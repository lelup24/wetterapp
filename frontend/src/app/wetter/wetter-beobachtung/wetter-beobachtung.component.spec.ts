import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WetterBeobachtungComponent } from './wetter-beobachtung.component';

describe('WetterBeobachtungComponent', () => {
  let component: WetterBeobachtungComponent;
  let fixture: ComponentFixture<WetterBeobachtungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WetterBeobachtungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WetterBeobachtungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
