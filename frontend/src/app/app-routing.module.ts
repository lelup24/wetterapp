import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WetterBeobachtungComponent } from './wetter/wetter-beobachtung/wetter-beobachtung.component';
import { WetterComponent } from './wetter/wetter.component';


const routes: Routes = [
  { path: '', component: WetterComponent },
  { path: 'beobachtung', component: WetterBeobachtungComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
