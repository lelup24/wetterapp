import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WetterComponent } from './wetter/wetter.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ZusammenfassungWetterComponent } from './wetter/zusammenfassung-wetter/zusammenfassung-wetter.component';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';

import { registerLocaleData } from '@angular/common';
import localeEN from '@angular/common/locales/en';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VorschauWetterComponent } from './wetter/vorschau-wetter/vorschau-wetter.component';

import { ChartsModule } from 'ng2-charts';
import { WetterBeobachtungComponent } from './wetter/wetter-beobachtung/wetter-beobachtung.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

registerLocaleData(localeEN, 'en');


@NgModule({
  declarations: [
    AppComponent,
    WetterComponent,
    ZusammenfassungWetterComponent,
    VorschauWetterComponent,
    WetterBeobachtungComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    ChartsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
